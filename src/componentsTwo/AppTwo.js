import React, {useEffect, useState} from 'react';

import {EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import './App.css';
import {ShowCode} from "../components/ShowCode";

export const AppTwo = () => {

    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [text, setText] = useState('');
    const [load, setLoad] = useState(false);
    const [dataLoad, setDataLoad] = useState(localStorage.getItem('value'));

    useEffect(() => {

        if (dataLoad !== null) {

            setDataLoad(dataLoad);
            setDataLoad(localStorage.getItem('value'));
            setText(localStorage.getItem('value'));
            console.log(localStorage.getItem('value'));

        }

    }, [dataLoad]);


    // // code temporal
    // const handleChange = (rawDraftContentState)=>{
    //     console.log(rawDraftContentState);
    // }

    const handleSaveData = () => {
        setText(editorState.getCurrentContent().getPlainText());
        setLoad(true);
        localStorage.setItem('value', editorState.getCurrentContent().getPlainText());
    }

    const handleDeleteData = () => {
        console.log('delete home')
        setDataLoad(null);
        setText('')
        localStorage.setItem('value', '');
        console.log('delete end')
    }

    const handleEditData = ()=>{
        setDataLoad(null);
        localStorage.setItem('value', '');
    }

    return (

        <div className="spacing">

            {(dataLoad === '' || dataLoad === null ) &&

            <div>
                <Editor
                    defaultEditorState={editorState}
                    onEditorStateChange={editorState => {
                        setEditorState(editorState);
                    }}
                    // onChange={handleChange}
                    wrapperClassName="wrapper-class"
                    editorClassName="editor-class"
                    toolbarClassName="toolbar-class"
                />

                <button className="btn" onClick={handleSaveData}>Save Data</button>
            </div>

            }

            {(load || dataLoad) &&

            <div>

                <button className="btn-delete spacing"
                        onClick={handleDeleteData}>Delete Data</button>

                <ShowCode code={text}/>
            </div>

            }


        </div>

    )
};