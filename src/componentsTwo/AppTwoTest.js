import React, {useState} from 'react';

import {EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import {App} from "../components/ShowCode";

export const AppTwo = () => {

    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [text, setText] = useState('');

    const [load, setLoad] = useState(false);

    const changeBodyHandler = (value) => {
        let item = value.blocks.map(x => x.text).join("\n");
        console.log({item});
        console.log({value});


        setEditorState({
            value
        });

        // console.log(editorState)
    };

    const handleChange = (rawDraftContentState)=>{
        console.log(rawDraftContentState);
    }

    const handleClick = ()=>{

        // console.log({editorState})
        console.log(editorState.getCurrentContent().getPlainText());

        setText(editorState.getCurrentContent().getPlainText());
        setLoad(true);

    }

    return (

        <div>

            {/*<Editor value={editorState} onChange={changeBodyHandler}/>*/}

            <Editor
                defaultEditorState={editorState}
                onEditorStateChange={editorState => {
                    setEditorState(editorState);
                    handleChange(editorState);
                }}
                wrapperClassName="wrapper-class"
                editorClassName="editor-class"
                toolbarClassName="toolbar-class"
            />

            <button onClick={handleClick}>Save</button>

            { load && <App code={text}/>}

        </div>


    )
};











//
// import React, {useState} from 'react';
// import { EditorState } from 'draft-js';
// import { Editor } from 'react-draft-wysiwyg';
// import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// // import axios from 'axios';
//
// export const AppTwo = () => {
//
//     const [editorState, setEditorState] = useState(EditorState.createEmpty());
//
//     const handleChange = (e) => {
//         this.setState({[e.target.name]: e.target.value})
//     }
//
//     const onEditorStateChange = () => {
//         setEditorState({
//             editorState
//         });
//     }
//
//     const handleSubmit = (e)=>{
//         e.preventDefault();
//
//         // const data = {
//         //     content: this.state.editorState
//         // };
//         //
//         // axios.post('http://localhost:5555/posts', {data})
//         //     .then(res => {
//         //         console.log(data);
//         //     })
//         //
//         // this.setState({editorState: EditorState.createEmpty()})
//     }
//
//
//     return (
//
//         <div>
//             <h5>Create document</h5>
//             <Editor
//                 editorState={editorState}
//                 wrapperClassName="demo-wrapper"
//                 editorClassName="demo-editor"
//                 onEditorStateChange={onEditorStateChange}
//             />
//             <button onClick={handleSubmit} className="btn-large waves-effect waves-light xbutton">Save</button>
//         </div>
//
//     )
// };
