import React, {useState} from 'react';
import {CopyBlock, dracula} from "react-code-blocks";

import {TopBar} from './TopBar';

import './styles.css';
import {arrayLanguages} from "./codeblocks";

export const App = () => {

    const [language, changeLanguage] = useState("jsx");
    const [languageDemo, changeDemo] = useState(arrayLanguages["jsx"]);
    const [lineNumbers, toggleLineNumbers] = useState(true);


    return (

        <div className="container mx-auto p-4">

            <TopBar
                language={{
                    value: language,
                    onChange: e => {
                        changeDemo(arrayLanguages[e.target.value]);
                        return changeLanguage(e.target.value);
                    },
                    options: Object.keys(arrayLanguages).map(lang => (
                        <option key={lang} value={lang}>
                            {lang}
                        </option>
                    ))
                }}
                toggle={{
                    checked: lineNumbers,
                    onChange: e => toggleLineNumbers(!lineNumbers)
                }}
            />
            <div className="demo">
                <CopyBlock
                    language={language}
                    text={languageDemo}
                    showLineNumbers={lineNumbers}
                    theme={dracula}
                    wrapLines={true}
                    codeBlock
                />

            </div>
        </div>

    )


};