// Import the functions you need from the SDKs you need
import {initializeApp} from 'firebase/app';
import {GoogleAuthProvider, FacebookAuthProvider} from 'firebase/auth';
import {getFirestore} from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDtdURU8VLY1npUDU97rDYfPuW8G_6xE2s",
    authDomain: "react-journal-bab9c.firebaseapp.com",
    projectId: "react-journal-bab9c",
    storageBucket: "react-journal-bab9c.appspot.com",
    messagingSenderId: "422779589075",
    appId: "1:422779589075:web:9f2b276d45f10150075f26"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const googleAuthProvider = new GoogleAuthProvider();
const facebookAuthProvider = new FacebookAuthProvider();

export {
    db,
    googleAuthProvider,
    facebookAuthProvider,
    // firebase
}