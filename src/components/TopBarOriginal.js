import React from "react";
import {Toggle} from "./Toggle";
import {Select} from "./Select";

export const TopBar = ( {toggle, language }) => {
    return (

        <div className="list-reset flex flex-wrap items-center justify-between my-2">
            <Toggle {...toggle} />
            <Select {...language} />
        </div>

    )
};
