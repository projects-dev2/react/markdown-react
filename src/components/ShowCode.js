import React, {useState} from 'react';
import {CopyBlock, dracula} from "react-code-blocks";

import {TopBar} from './TopBar';

import './styles.css';

export const ShowCode = ({code}) => {

    const language = "jsx";
    const [lineNumbers, toggleLineNumbers] = useState(true);

    return (

        <div className="container mx-auto p-4">

            <TopBar
                toggle={{
                    checked: lineNumbers,
                    onChange: e => toggleLineNumbers(!lineNumbers)
                }}
            />

            <div className="demo">
                <CopyBlock
                    language={language}
                    text={code}
                    showLineNumbers={lineNumbers}
                    theme={dracula}
                    wrapLines={true}
                    codeBlock
                />
            </div>

        </div>

    )

};