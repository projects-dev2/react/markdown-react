export const arrayLanguages =  {
    cpp: `#include <iostream.h>
main() {
  cout << "Hello World!" << endl;
  return 0;
} `,
    csharp: `class HelloWorld {
 static void Main() {
  System.Console.WriteLine("Hello, World!");
 }
}`,
    dart: `main() {
  print("Hello world!");
}
`,
    graphql: `query FirstSevenStarShips {
  allStarships(first: 7) {
    edges {
      node {
        id
        name
        model
        costInCredits
      }
    }
}
`,
    go: `package main
import "fmt"
func main() {
  fmt.Printf("Hello World\n")
}
`,
    html: `<html>
<!-- Hello World in HTML -->
<head>
<title>Hello World!</title>
</head>
<body>
Hello World!
</body>
</html>
`,
    java: `class HelloWorld {
  static public void main( String args[] ) {
    System.out.println( "Hello World!" );
  }
}`,
    javascript: `var sys = require("sys");
sys.puts("Hello World");
`,
    jsx: `class HelloMessage extends React.Component {
  handlePress = () => {
    alert('Hello')
  }
  render() {
    return (
      <div>
        <p>Hello {this.props.name}</p>
        <button onClick={this.handlePress}>Say Hello</button>
      </div>
    );
  }
}

ReactDOM.render(
  <HelloMessage name="Taylor" />, 
  mountNode 
);`,
    kotlin: `fun main(args : Array<String>) {
  println("Hello, world!")
}
`,
    php: `<?php
  echo "Hello World!";
?>
`,
    python: `# Hello world in Python 2
print "Hello World"

# Hello world in Python 3 (aka Python 3000)
print("Hello World")
`,
    sql: `SELECT "Hello World";`,
    swift: `println("Hello, world!")`,
    tsx: `import * as React from "react";

export class HelloWorld extends React.Component<any, any> {
    render() {
        return <div>Hello world!It's from Helloword Component.</div>;
    }
}`,
    typescript: `var exclamation: string = "Hello";
var noun: string = "World";
console.log(exclamation + noun);
`
};
