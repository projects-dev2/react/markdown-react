import React from 'react';
import ReactDOM from 'react-dom';

import {AppTwo} from "./componentsTwo/AppTwo";

ReactDOM.render(
    <AppTwo/>,
    document.getElementById('root')
);